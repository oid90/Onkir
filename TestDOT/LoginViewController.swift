//
//  ViewController.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let defautls = UserDefaults.standard
    
    @IBOutlet weak var usernameTextField: UITextField?
    @IBOutlet weak var passwordTextField: UITextField?
    @IBOutlet weak var loginButton: ButtonStyle!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red:0.11, green:0.53, blue:0.83, alpha:1.00)
        
        loginButton.roundRectColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00)
        loginButton.setTitleColor(UIColor.white, for: [])
    
    }
    
    @IBAction func loginAction(_ sender: UIButton){
        guard let _ = usernameTextField?.text else {return}
        guard let _ = passwordTextField?.text else {return}
        defautls.set(true, forKey: Constants.is_logged_in)
        if let mainVC = storyboard?.instantiateViewController(withIdentifier: "main_vc"){
            let navController = UINavigationController(rootViewController: mainVC)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    
}

