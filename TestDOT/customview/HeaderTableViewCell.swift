//
//  HeaderTableViewCell.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/24/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!

    var item: CheckViewModelItem? {
        didSet{
            guard let item = item as? CheckHeaderModel else {return}
            nameLabel.text = item.name
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
    
}
