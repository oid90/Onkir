//
//  ButtonStyle.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit

@IBDesignable
class ButtonStyle: UIButton {
    
    @IBInspectable public var roundRectCornerRadius: CGFloat = 3{
        didSet{
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable public var roundRectColor: UIColor = UIColor.clear{
        didSet{
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable public var borderColor: UIColor = UIColor.clear{
        didSet{
            self.setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = roundRectCornerRadius
        layer.backgroundColor = roundRectColor.cgColor
        layer.borderWidth = 0.5
        layer.borderColor = borderColor.cgColor
    }
    
    private var roundRectLayer:CAShapeLayer?
    
    private func layoutRoundRectLayer(){
        if let existingLayer = roundRectLayer {
            existingLayer.removeFromSuperlayer()
        }
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: roundRectCornerRadius).cgPath
        shapeLayer.fillColor = roundRectColor.cgColor
        self.layer.insertSublayer(shapeLayer, at: 0)
        self.roundRectLayer = shapeLayer
        
    }
}
