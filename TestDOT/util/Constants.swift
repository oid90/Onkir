//
//  Constants.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation

struct Constants {
    static let baseUrl = "https://api.rajaongkir.com/starter/"
    static let apiKey = "0df6d5bf733214af6c6644eb8717c92c"
    static let contentType = "application/x-www-form-urlencoded; charset=utf-8"
    static let is_logged_in = "is_logged_in"
}
