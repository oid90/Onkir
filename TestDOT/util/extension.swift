//
//  extension.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/24/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation

extension String{
    static func priceFormat(_ price: NSNumber) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = "Rp "
        formatter.locale = Locale(identifier: "id")
        return formatter.string(from: price)!
    }
}
