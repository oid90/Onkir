//
//  RealmManager.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager<T: Object> {
    
    let realm: Realm
    enum RealmResult{
        case finished
    }
    
    init() {
        realm = try! Realm()
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    func addData(_ model: T, _ completion: @escaping(RealmResult) -> Void){
        try! realm.write {
            realm.add(model)
        }
        completion(.finished)
    }
    
    func addDatas(_ models: [T], _ completion: @escaping(RealmResult) -> Void){
        try! realm.write {
            for model in models{
                realm.add(model)
            }
        }
        completion(.finished)
    }
    
    func fetchObjects(_ model: T.Type, _ searchKey: String, _ id: Int ) -> [T?] {
        let objs = realm.objects(model).filter("\(searchKey) == %@", String(describing: id))
        return Array(objs)
    }
    
    func fetchObjects(_ model: T.Type) -> [T?] {
        let objs = realm.objects(model)
        return Array(objs)
    }
    
    func filterObjects(_ model: T.Type, _ searchKey: String, _ searchValue: String) -> [T?] {
        let objs = realm.objects(model).filter("\(searchKey) CONTAINS %@", searchValue)
        return Array(objs)
    }
}
