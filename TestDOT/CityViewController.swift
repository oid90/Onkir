//
//  CityViewController.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit

class CityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    var provinceID = 0
    var cityModel: CityRealm!
    var arrCity = [CityRealm]()
    var filteredCity = [CityRealm]()
    var manager = RealmManager<CityRealm>()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.layer.cornerRadius = 10.0
        containerView.layer.borderColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00).cgColor
        containerView.layer.borderWidth = 2.0
        
        getCity(provinceID)
    }
    
    func getCity(_ provinceID: Int){
        DispatchQueue.main.async {
            let objects = self.manager.fetchObjects(CityRealm.self, "province_id", provinceID)
            self.arrCity = objects as! [CityRealm]
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredCity.removeAll()
        if let searchText = searchBar.text{
            filteredCity = arrCity.filter({
                $0.city_name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            })
        }
        self.tableView.reloadData()
    }
    
}

extension CityViewController{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !filteredCity.isEmpty {
            return filteredCity.count
        }
        return arrCity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "city_cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
    
        var city: CityRealm
        if !filteredCity.isEmpty {
            city = filteredCity[indexPath.row]
        }else{
            city = arrCity[indexPath.row]
        }
        
        cell.textLabel?.text = city.city_name
        cell.detailTextLabel?.text = city.type
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if !filteredCity.isEmpty{
            cityModel = filteredCity[indexPath.row]
        }else{
            cityModel = arrCity[indexPath.row]
        }
        
        performSegue(withIdentifier: "back_segue", sender: self)
    }
    
}
