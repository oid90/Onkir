//
//  CityRealm.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation
import RealmSwift

class CityRealm : Object{
    @objc dynamic var city_id = ""
    @objc dynamic var province_id = ""
    @objc dynamic var province = ""
    @objc dynamic var type = ""
    @objc dynamic var city_name = ""
    @objc dynamic var postal_code = ""
}
