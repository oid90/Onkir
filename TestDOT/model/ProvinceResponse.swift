//
//  ProvinceResponse.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation

struct ProvinceResponse: Decodable {
    let rajaongkir: RajaOngkir
}

struct RajaOngkir: Decodable {
    let status: Status
    let results: [Result]
}

struct Status: Decodable {
    let code: Int
    let description: String
}

struct Result: Decodable {
    let province_id: String
    let province: String
}
