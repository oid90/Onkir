//
//  ProvinceRealm.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation
import RealmSwift

class ProvinceRealm: Object{
    @objc dynamic var provinceId = 0
    @objc dynamic var province = ""

}
