//
//  CityResponse.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation

struct CityResponse: Decodable {
    let rajaongkir: CityRajaOngkir
}

struct CityRajaOngkir: Decodable {
    let status: Status
    let results: [CityResult]
}

struct CityResult: Decodable {
    let city_id: String
    let province_id: String
    let province: String
    let type: String
    let city_name: String
    let postal_code: String
}
