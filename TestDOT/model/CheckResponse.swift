//
//  CheckResponse.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/24/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation

struct CheckResponse: Decodable{
    var rajaongkir: CheckRajaOngkir
}

struct CheckRajaOngkir: Decodable{
    var origin_details: LocationDetail
    var destination_details: LocationDetail
    var results: [CheckResult]
}

struct LocationDetail: Decodable{
    var city_id: String
    var province_id: String
    var province: String
    var type: String
    var city_name: String
    var postal_code: String
}

struct CheckResult: Decodable{
    var code: String
    var name: String
    var costs: [Costs]
}

struct Costs: Decodable{
    var service: String
    var description: String
    var cost: [Cost]
}

struct Cost: Decodable{
    var value: Int
    var etd: String
    var note: String
    
    var price: String{
        get{ return String.priceFormat(NSNumber(value: value))}
    }
}
