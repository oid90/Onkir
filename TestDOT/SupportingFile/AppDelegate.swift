//
//  AppDelegate.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let mainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
    let defaults = UserDefaults.standard

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = UIColor(red:0.11, green:0.53, blue:0.83, alpha:1.00)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        removeRealm()
        
        checkLoggedInUser()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func removeRealm(){
        do{
            try FileManager.default.removeItem(at: Realm.Configuration.defaultConfiguration.fileURL!)
        }catch {}
    }
    
    func checkLoggedInUser(){
        let isLoggedIn = defaults.bool(forKey: Constants.is_logged_in)
        if !isLoggedIn{
            if let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "login_vc") as? LoginViewController{
                self.window?.rootViewController = loginVC
            }
        } else {
            if let homeVC = mainStoryBoard.instantiateViewController(withIdentifier: "main_vc") as? TrackViewController{
                let navController = UINavigationController(rootViewController: homeVC)
                self.window?.rootViewController = navController
            }
            
        }
    }

}

