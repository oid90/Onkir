//
//  TrackViewController.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit
import Alamofire
import APESuperHUD

struct TrackModel {
    var dictCityID = ["origin": "",
                      "destination": ""]
    var weight = 0
    var courier = ""
}

class TrackViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var buttonOrigin: ButtonStyle!
    @IBOutlet weak var buttonDestination: ButtonStyle!
    @IBOutlet weak var buttonCheck: ButtonStyle!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var buttonEkspedisi: ButtonStyle!
    
    var senderTag = 0
    var model = TrackModel()
    let viewModel = TrackViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        APESuperHUD.show(style: .loadingIndicator(type: .standard), title: nil, message: "Tunggu sebentar...", completion: nil)
        viewModel.getData { (result) in
            switch result{
            case .finished:
                APESuperHUD.dismissAll(animated: true)
                break
            }
        }
    }
    
    func setupUI(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.title = "Home"
        
        self.view.backgroundColor = UIColor(red:0.11, green:0.53, blue:0.83, alpha:1.00)
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 10
        
        buttonOrigin.borderColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00)
        buttonOrigin.roundRectCornerRadius = 5
        buttonOrigin.addTarget(self, action: #selector(gotoProvince(_:)), for: .touchUpInside)
        
        buttonDestination.borderColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00)
        buttonDestination.roundRectCornerRadius = 5
        buttonDestination.addTarget(self, action: #selector(gotoProvince(_:)), for: .touchUpInside)
        
        buttonCheck.roundRectColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00)
        buttonCheck.setTitleColor(UIColor.white, for: [])
        buttonCheck.roundRectCornerRadius = 5
        
        buttonEkspedisi.borderColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00)
        buttonEkspedisi.roundRectCornerRadius = 5
        buttonEkspedisi.setTitleColor(UIColor.black, for: [])
        
        weightTextField.layer.borderColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00).cgColor
        weightTextField.layer.borderWidth = 0.5
        weightTextField.layer.cornerRadius = 5
        
    }
    
    @objc func gotoProvince(_ sender: UIButton){
        senderTag = sender.tag
        performSegue(withIdentifier: "province_segue", sender: self)
    }
    
    @IBAction func btnLogOut(_ sender: UIBarButtonItem){
        UserDefaults.standard.set(false, forKey: Constants.is_logged_in)
        if let loginVC = storyboard?.instantiateViewController(withIdentifier: "login_vc") as? LoginViewController{
            self.present(loginVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func checkAction(_ sender: UIButton){
        guard let weight = Int(weightTextField.text!)  else { return }
        guard let courier = buttonEkspedisi.titleLabel?.text else {return}
        model.weight = weight
        model.courier = courier
        
        if (model.dictCityID["origin"]?.isEmpty)! {
            print("please fill origin before")
        }
        
        if (model.dictCityID["destination"]?.isEmpty)!{
            print("please fill destination before")
        }
        
        self.resignFirstResponder()
        
        performSegue(withIdentifier: "check_segue", sender: self)
    }
    
    @IBAction func backToTracking(_ segue: UIStoryboardSegue){
        if let sourceVC = segue.source as? CityViewController{
            if senderTag == 1{
                buttonOrigin.setTitle(sourceVC.cityModel.city_name, for: [])
                model.dictCityID["origin"] = sourceVC.cityModel.city_id
            }else if senderTag == 2{
                buttonDestination.setTitle(sourceVC.cityModel.city_name, for: [])
                model.dictCityID["destination"] = sourceVC.cityModel.city_id
            }
        }
        
        if let sourceVC = segue.source as? EkspedisiViewController{
            buttonEkspedisi.setTitle(sourceVC.selectedItem.uppercased(), for: [])
            model.courier = sourceVC.selectedItem
        }
    }

    @IBAction func close(_ segue: UIStoryboardSegue) { }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "province_segue" {
            let destinationVC = segue.destination as! ProvinceViewController
            if senderTag == 1 {
                destinationVC.type = "origin"
            }else if senderTag == 2 {
                destinationVC.type = "destination"
            }
        }
        
        if segue.identifier == "check_segue"{
            let destinationVC = segue.destination as! CheckOngkirViewController
            destinationVC.model = model
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
