//
//  CheckOngkirViewController.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/24/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit
import APESuperHUD

class CheckOngkirViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var model: TrackModel!
    let viewModel = CheckOngkirViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Check"
        setupUI()
    
        APESuperHUD.show(style: .loadingIndicator(type: .standard), title: nil, message: "Tunggu sebentar...", completion: nil)
        viewModel.checkOngkir(model: model) { (result) in
            switch result{
            case .success:
                APESuperHUD.dismissAll(animated: true)
                self.tableView?.reloadData()
                break
            case .failure:
                APESuperHUD.dismissAll(animated: true)
                break
            }
        }
    }
    
    func setupUI(){
        tableView.register(HeaderTableViewCell.nib, forCellReuseIdentifier: HeaderTableViewCell.identifier)
        tableView.register(CostTableViewCell.nib, forCellReuseIdentifier: CostTableViewCell.identifier)
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: self.tableView.bounds.height))
        footerView.backgroundColor = UIColor.white
        self.tableView?.tableFooterView = footerView
    }

}

extension CheckOngkirViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items[section].rowCount
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        //create view
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 35))
        view.backgroundColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00)
        
        //set title
        let titleLabel = UILabel(frame: CGRect(x: 10 , y: 8, width: tableView.frame.width, height: 18))
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0),
                         NSAttributedStringKey.foregroundColor: UIColor.white]
        let attributedString = NSAttributedString(string: viewModel.items[section].sectiontitle, attributes: attributes)
        titleLabel.attributedText = attributedString
        
        view.addSubview(titleLabel)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.items[indexPath.section]
        switch item.type {
        case .header:
            if let cell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.identifier, for: indexPath) as? HeaderTableViewCell{
                cell.item = item
                return cell
            }
        case .costs:
            if let item = item as? CheckCostModel, let cell = tableView.dequeueReusableCell(withIdentifier: CostTableViewCell.identifier, for: indexPath) as? CostTableViewCell{
                cell.item = item.costs[indexPath.row]
                return cell
            }
        }
        return UITableViewCell()
    }
    
}
