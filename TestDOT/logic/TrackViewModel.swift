//
//  TrackViewModel.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class TrackViewModel {
    
    var dbManager = RealmManager()
    
    enum TrackViewResult{
        case finished
    }

    //Get Data
    func getData(_ completion: @escaping(TrackViewResult) -> Void){
        
        let urlProvince = "province"
        let urlCity = "city"
        let headers: HTTPHeaders = ["Content-Type": Constants.contentType,
                                    "key": Constants.apiKey]
        
        Alamofire.request(Constants.baseUrl.appending(urlProvince), method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: headers).responseData(completionHandler: { (data) in
            switch data.result{
            case .success(let value):
                do{
                    let provinceResponse = try JSONDecoder().decode(ProvinceResponse.self, from: value)
                    
                    var models = [ProvinceRealm]()
                    for result in provinceResponse.rajaongkir.results {
                        let model = ProvinceRealm()
                        model.provinceId = Int(result.province_id)!
                        model.province = result.province
                        models.append(model)
                    }
                    
                    self.dbManager.addDatas(models, { (result) in
                        switch result{
                        case .finished:
                            print("wait for city data")
                            break
                        }
                    })
                    
                }catch let jsonError {
                    print("Error parsing json", jsonError)
                }
                break
            case .failure(let error):
                print(error)
            }
        })
        
        Alamofire.request(Constants.baseUrl.appending(urlCity), method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: headers).responseData { (response) in
            switch response.result{
            case .success(let data):
                do{
                    let cityResponse = try JSONDecoder().decode(CityResponse.self, from: data)
                    var cities = [CityRealm]()
                    cityResponse.rajaongkir.results.forEach({ (result) in
                        let model = CityRealm()
                        model.province_id = result.province_id
                        model.province = result.province
                        model.city_id = result.city_id
                        model.city_name = result.city_name
                        model.type = result.type
                        model.postal_code = result.postal_code
                        cities.append(model)
                    })
                    self.dbManager.addDatas(cities, { (result) in
                        switch result{
                        case .finished:
                            completion(.finished)
                            break
                        }
                    })
                }catch let jsonError{
                    print("Error parsing json", jsonError)
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }

}
