//
//  CheckOngkirViewModel.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/24/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import Foundation
import Alamofire

enum CheckViewModelItemType{
    case header
    case costs
}

enum CheckOngkirResult{
    case success
    case failure
}

protocol CheckViewModelItem {
    var type: CheckViewModelItemType { get }
    var sectiontitle: String { get }
    var rowCount: Int { get }
}

class CheckOngkirViewModel{
    
    var items = [CheckViewModelItem]()
    
    let headers: HTTPHeaders = ["Content-Type": Constants.contentType,
                                "key": Constants.apiKey]
    var params: Parameters = ["origin": 0,
                              "destination": 0,
                              "weight": 0,
                              "courier": ""]

    func checkOngkir(model param: TrackModel?, _ completion: @escaping(CheckOngkirResult) -> Void){
        
        guard let param = param else { return }
        let originID = Int(param.dictCityID["origin"]!)
        let destiantionID = Int(param.dictCityID["destination"]!)
        
        params["origin"] = originID
        params["destination"] = destiantionID
        params["weight"] = param.weight
        params["courier"] = String(describing: param.courier.lowercased())
    
        let url = "cost"
        Alamofire.request(Constants.baseUrl.appending(url), method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseData { (response) in
            switch response.result{
            case .success(let value):
                do{
                    let dataResponse = try JSONDecoder().decode(CheckResponse.self, from: value)
                    self.collectData(dataResponse)
                }catch let jsonError {
                    print("failed to parse json \(jsonError)")
                }
                completion(.success)
                break
            case .failure(let error):
                print("error \(error)")
                completion(.failure)
                break
            }
        }
    }
    
    func collectData(_ data: CheckResponse){
        
        data.rajaongkir.results.forEach { (result) in
            items.append(CheckHeaderModel(code: result.code, name: result.name))
            items.append(CheckCostModel(costs: result.costs))
        }
    }
}

class CheckHeaderModel: CheckViewModelItem{
    var type: CheckViewModelItemType{
        return .header
    }
    
    var sectiontitle: String{
        return "Ekspedisi"
    }
    
    var rowCount: Int{
        return 1
    }
    
    var code: String
    var name: String
    
    init(code: String, name: String) {
        self.code = code
        self.name = name
    }
}

class CheckCostModel: CheckViewModelItem{

    var type: CheckViewModelItemType{
        return .costs
    }
    
    var sectiontitle: String{
        return "Paket"
    }
    
    var rowCount: Int{
        return costs.count
    }
    
    var costs: [Costs]
    
    init(costs: [Costs]) {
        self.costs = costs
    }
}
