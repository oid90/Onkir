//
//  ProvinceViewController.swift
//  TestDOT
//
//  Created by Ilham Hadi Prabawa on 7/23/18.
//  Copyright © 2018 Ilham Hadi Prabawa. All rights reserved.
//

import UIKit

class ProvinceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    var type = ""
    var arrProvince = [ProvinceRealm]()
    var filteredProvince = [ProvinceRealm]()
    var manager = RealmManager<ProvinceRealm>()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeView()
        getProvince()
    }
    
    func customizeView(){
        containerView.layer.cornerRadius = 10.0
        containerView.layer.borderColor = UIColor(red:0.18, green:0.77, blue:0.38, alpha:1.00).cgColor
        containerView.layer.borderWidth = 2.0
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredProvince.removeAll()
        if let searchText = searchBar.text{
            filteredProvince = manager.filterObjects(ProvinceRealm.self, "province", searchText) as! [ProvinceRealm]
        }
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "city_segue"{
            if let indexPath = self.tableView.indexPathForSelectedRow{
                let destinationVC = segue.destination as! CityViewController
                var province: ProvinceRealm
                if !filteredProvince.isEmpty{
                    province = self.filteredProvince[indexPath.row]
                }else{
                    province = self.arrProvince[indexPath.row]
                }
                destinationVC.provinceID = province.provinceId
            }
        }
    }
    
    func getProvince(){
        DispatchQueue.main.async {
            self.arrProvince = self.manager.fetchObjects(ProvinceRealm.self) as! [ProvinceRealm]
            self.tableView.reloadData()
        }
    }

}

extension ProvinceViewController{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !filteredProvince.isEmpty{
            return filteredProvince.count
        }
        return arrProvince.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "province_cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        var model: ProvinceRealm
        if !filteredProvince.isEmpty {
            model = filteredProvince[indexPath.row]
        }else{
            model = arrProvince[indexPath.row]
        }
        cell.textLabel?.text = model.province
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
